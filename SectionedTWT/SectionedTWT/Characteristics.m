(* ::Package:: *) 

BeginPackage["SectionedTWT`Characteristics`"]; 
(* Exported symbols added here with SymbolName::usage *) 

Depression::usage = 
"Depression[h, a, Nph]"; 

Dispersion::usage = 
"Dispersion[data][f]"; 

RPierce::usage = 
"RPierce[data][f]"; 

Dissipation::usage = 
"Dissipation[data]";

DepressionNum::usage = 
"DeprNum[a, HBeam, Veam]"; 

Begin["`Private`"]; (* Begin Private Context *) 

VoltageToVelocity[V_Real] := 
With[{gamma = 1.0 + V / 510998.928}, 
	(*Return*) 
	((gamma^2 - 1.0) / gamma) * 299972458.0 
]; 

(*  *) 
Dispersion[data_] := Interpolation[Map[{10^9, 1}#&, data[[All, {1, 2}]]]];
RPierce[data_] := Interpolation[Map[{10^9, 1}#&, data[[All, {1, 3}]]]]; 
Dissipation[data_] := Interpolation[Map[{10^9, 1}#&, data[[All, {1, 4}]]]];

(* depression *)

DepressionNum[a_, b1_, gamma_] := 
1 - Sinh[b1 gamma] Cosh[gamma (a - b1)] / (b1 * gamma * Cosh[gamma a]); 

gamma[k_, Nph_] := k Sqrt[Nph^2 - 1];

b1[Hbeam_] := Hbeam / 2;

k[omega_] := omega / c;

c = 299972458.0;

Depression[Hbeam_, ABeam_, Nph_] := DepressionNum[ABeam, b1[Hbeam], gamma[k[2Pi #], Nph]]&

End[]; (* End Private Context *) 

EndPackage[]; 