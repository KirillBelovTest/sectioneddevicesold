(* ::Package:: *)

(* ::Section:: SectionedTWT *)

BeginPackage["SectionedTWT`"]
(* Exported symbols added here with SymbolName::usage *)  

(* ::Subsection:: Phisical constants *)

c::usage = "speed of light"
c = 299792458.0

e::usage = "elementary charge"
e = 1.6021766208 * 10^-19

m::usage = "electron mass"
m = 9.10938356 * 10^-31

eps::usage = "vacuum permittivity"
eps = 8.85418781762 * 10^-12

(* ::Subsection:: Parameters of the tube *)

Wb::usage = "weight of the beam, m"
Wb = 750.0 * 10^-6

Hb::usage = "height of the beam, m"
Hb = 100.0 * 10^-6

V0::usage = "voltage, V"
V0 = 20000.0

b1::usage = "half height of the beam, m"
b1 = Hb / 2

a1::usage = "half height of the flyway canal, m"
a1 = 100.0 * 10^6

Sb::usage = "beam area, m^2"
Sb = Wb * Hb

I0::usage = "beam current, A"
I0 = 0.1

l::usage = "length of the tube, m"
l = 0.04

(* ::Subsection:: Basic formulas and definitions *)

omega::usage = "omega[f] - angle frequency"
omega[f_?NumericQ] := 2 Pi * f

k::usage = "k[omega] - wave number"
k[omega_?NumericQ] := omega / c

lambda::usage = "lambda[f] - wavelength"
lambda[f_?NumericQ] := c / f

gamma0::usage = "gamma0[V0] - relativistic mass factor"
gamma0[V0_?NumericQ] := 1 + (e * V0) / (m * c^2)

v0::usage = "v0[gamma0] - initial velocity of the electron beam"
v0[gamma0_?NumericQ] := c Sqrt[1 - 1 / (gamma0^2)]

omegap::usage = "omegap[I0, Sb, v0] - plasma frequency"
omegap[I0_?NumericQ, Sb_?NumericQ, v0_?NumericQ] := Sqrt[(e I0) / (m eps v0 Sb)]

betae::usage = "betae[omega, v0] - electronic propagation constant"
betae[omega_?NumericQ, v0_?NumericQ] := omega / v0

gamma::usage = "gamma[k, Nph] - transverse gamma factor"
gamma[k_?NumericQ, Nph_?NumericQ] := k Sqrt[Nph^2 - 1]

bgamma::usage = "bgamma[b1, gamma] - transverse gamma factor with b"
bgamma[b1_?NumericQ, gamma_?NumericQ] := b1 * gamma



Begin["`Private`"] (* Begin Private Context *) 

End[] (* End Private Context *)

EndPackage[]