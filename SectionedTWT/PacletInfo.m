(* Paclet Info File *)

Paclet[
    Name -> "NonlinearTWT",
    Version -> "0.0.2",
    MathematicaVersion -> "10+",
    Creator -> "Nikita M. Ryskin, Andrey G. Roznev, Kirill Belov",
    Extensions -> 
        {
            {"Kernel", Root -> "/"}
        }
]


