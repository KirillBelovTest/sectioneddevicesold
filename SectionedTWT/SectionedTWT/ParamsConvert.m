(* ::Package:: *)

(* :Title: ParamsConvert *) 
(* :Context: SectionedTWT`ParamsConvert` *) 
(* :Creator: Kirill Belov *) 

BeginPackage["SectionedTWT`ParamsConvert`"]; 

TWTDimensionlessParams::usage = 
"TWTDimensionlessParams[{\"VBeam\" -> 20000.0, ...}]"; 

Begin["`Private`"]; 

TWTDimensionlessParams[params: {__Rule}] := 
With[
	{
		c0 = 299792458.0, 
		m0 = 9.10938291 * 10^-31, 
		e0 = 1.602176565 * 10^-19, 
		eps0 = 8.854 10^-12, 
		
		VBeam = "VBeam" /. params, 
		IBeam = "IBeam" /. params, 
		SBeam = "SBeam" /. params, 
		
		FreqList = ("FreqList" /. params) * {1.0, 2.0}, 
		PowerList = "PowerList" /. params, 
		PhaseList = "PhaseList" /. params, 
		
		CoordList = "CoordList" /. params, 
		
		DispersionList = "DispersionList" /. params, 
		RPierceList = "RPierceList" /. params, 
		DissipationList = "DissipationList" /. params, 
		DepressionList = "DepressionList" /. params, 
		NF = "NF" /. params, 
		NH = "NH" /. params, 
		NP = "NP" /. params
	}, 
	Block[
		{
			gamma0, CList, betaEList, beta0, omegaList, lList, 
			bList, dList, deltaList, omegaP, q0, qList, PNormList, F0List
		}, 
		
		gamma0 = 1 + e0 VBeam / (m0 c0^2); 
		CList = Table[(IBeam RPierce / (4 VBeam))^(1/3), {RPierce, RPierceList}]; 
		
		omegaList = 2Pi FreqList; 
		
		beta0 = Sqrt[gamma0^2 - 1] / gamma0; 
		betaEList = Table[omegaList[[k]] / (beta0 c0), {k, 1, NF}]; 
		
		lList = Table[coord betaEList[[1]] CList[[1]], {coord, CoordList}]; 
		
		bList = Table[(beta0 DispersionList[[k]] - 1) / CList[[k]], {k, 1, NF}]; 
		
		dList = Table[DissipationList[[k]] / (8.68 betaEList[[k]] CList[[k]] k), {k, 1, NF}]; 
		
		deltaList = Table[RPierceList[[k]] k^2 (1 + CList[[k]] bList[[k]])^2, {k, 1, NF}] / RPierceList[[1]]; 
		
		omegaP = Sqrt[e0 IBeam / (m0 eps0 c0 beta0 SBeam)]; 
		q0 = (omegaP / (omegaList[[1]] CList[[1]]))^2; 

		qList = q0 Table[DepressionList[[k]], {k, 1, NH}]; 
		
		PNormList = (IBeam VBeam CList[[1]] RPierceList[[1]] / (2 beta0^2)) * 
		Table[1 / (DispersionList[[k]]^2 k^2 RPierceList[[k]]), {k, 1, NF}]; 
		
		F0List = Table[(PowerList[[k]] / PNormList[[k]]) * Exp[I PhaseList[[k]]], {k, 1, NF}]; 
		
		{"bList" -> bList, "dList" -> dList, "deltaList" -> deltaList, "qList" -> qList, 
		"CList" -> CList, "lList" -> lList, "PNormList" -> PNormList, "F0List" -> F0List, 
		"NF" -> NF, "NH" -> NH, "NP" -> NP}	
	]
]; 

End[]; (*`Private`*) 

EndPackage[]; (*SectionedTWT`ParamsConvert`*) 
