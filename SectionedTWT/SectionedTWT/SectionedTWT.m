(* ::Package:: *)

BeginPackage["SectionedTWT`", {"SectionedTWT`Characteristics`", "SectionedTWT`ParamsConvert`"}]

Unprotect["`*"]
Clear["`*"]

TWTSolverSection::usage = 
"TWTSolverSection[b, d, delta, h, q, F0, J0, Phi0, dPhi0, CC, {l0, l}, NF, NH, NP]"; 

TWTSolverDrift::usage = 
"TWTSolverDrift[q, J0, Phi0, dPhi0, CC, {l0, l}, NH, NP]"; 

TWTSolverMultiSectionAmplifier::usage = 
"TWTSolverMultiSectionAmplifier[]"; 

TWTSolverMultiSectionMultiplier::usage = 
"TWTSolverMultiSectionMultiplier[b, d, dl, q, F0, CList, {l0, l1, l2, l3}, NF, NH, NP]"; 

TWTParams::usage = "TWTParams[opts]"

Begin["`Private`"]; 

TWTSolverSection[
	b_List, d_List, dl_List, h_List, q_List, 
	F0_List, J0_List, Phi0_List, dPhi0_List, 
	CC_Real, {l0_Real, l_Real}, 
	NF_Integer, NH_Integer, NP_Integer
] := 

Module[
	{
		equations, 
		conditions, 
		functions, 
		
		FRe, FIm, JRe, JIm, Phi, x
	},
	
	equations = Flatten[
		{
			
			(* Field equations *)
			Table[
				{
					FRe[k]'[x] + k d[[k]] FRe[k][x] - k b[[k]] FIm[k][x] == -dl[[k]] h[[k]] JRe[k][x], 
					FIm[k]'[x] + k d[[k]] FIm[k][x] + k b[[k]] FRe[k][x] == -dl[[k]] h[[k]] JIm[k][x]
				}, 
				
				{k, 1, NF}
			], 
			
			(* Equations for the beam current *)
			Table[
				{
					JRe[k][x] == (2/NP) Sum[Cos[k Phi[i][x]], {i, 1, NP}], 
					JIm[k][x] == -(2/NP) Sum[Sin[k Phi[i][x]], {i, 1, NP}]
				}, 

				{k, 1, NH}
			], 
			
			(* Equations of motion *)
			Table[
				Phi[i]''[x] == -(1 + CC Phi[i]'[x])^3 * 
				(
					Sum[FRe[k][x] Cos[k Phi[i][x]] - FIm[k][x] Sin[k Phi[i][x]], 
					{k, 1, NF}] - 
				
					Sum[(q[[k]]/k) (JRe[k][x]Sin[k Phi[i][x]] + JIm[k][x] Cos[k Phi[i][x]]), 
					{k, 1, NH}]
				), 
			
				{i, 1, NP}
			]
		}
	];
	
	conditions = Flatten[
		{
			Table[
				{
					FRe[k][l0] == Im[F0[[k]]], 
					FIm[k][l0] == Re[F0[[k]]]
				}, 
			
				{k, 1, NF}
			], 
		
			Table[
				{
					JRe[k][l0] == Re[J0[[k]]],
					JIm[k][l0] == Im[J0[[k]]]
				}, 
			
				{k, 1, NH}
			], 
		
			Table[
				Phi[i][l0] == Phi0[[i]], 
				{i, 1, NP}
			], 

			Table[
				Phi[i]'[l0] == dPhi0[[i]], 
				{i, 1, NP}
			]

		}
	]; 
	
	functions = Flatten[
		{
			Table[{FRe[k], FIm[k]}, {k, 1, NF}], 
			Table[{JRe[k], JIm[k]}, {k, 1, NH}], 
			Table[Phi[i], {i, 1, NP}] 
		}
	];
	
	functions /. 
	First[NDSolve[Join[equations, conditions], functions, {x, l0, l}]]
]

TWTSolverDrift[
	q_List, 
	J0_List, Phi0_List, dPhi0_List, 
	CC_Real, {l0_Real, l_Real}, 
	NH_Integer, NP_Integer 
] := 

Module[ 
	{ 
		equations, 
		conditions, 
		functions, 
		
		JRe, JIm, Phi, x 
	},
	
	equations = Flatten[
		{ 
			(* Equations for the beam current *)
			Table[
				{
					JRe[k][x] == (2/NP) Sum[Cos[k Phi[i][x]], {i, 1, NP}], 
					JIm[k][x] == -(2/NP) Sum[Sin[k Phi[i][x]], {i, 1, NP}]
				}, 

				{k, 1, NH}
			], 
			
			(* Equation of motion *)
			Table[
				Phi[i]''[x] == (1 + CC Phi[i]'[x])^3 * 
				Sum[(q[[k]]/k) (JRe[k][x] Sin[k Phi[i][x]] + JIm[k][x] Cos[k Phi[i][x]]), {k, 1, NH}], 
			
				{i, 1, NP}
			]
		}
	];
	
	conditions = Flatten[
		{
		
			Table[
				{
					JRe[k][l0] == Re[J0[[k]]],
					JIm[k][l0] == Im[J0[[k]]]
				}, 
			
				{k, 1, NH}
			], 
		
			Table[
				Phi[i][l0] == Phi0[[i]], 
				{i, 1, NP}
			], 
		
			Table[
				Phi[i]'[l0] == dPhi0[[i]], 
				{i, 1, NP}
			]

		}
	]; 
	
	functions = Flatten[
		{
			Table[{JRe[k], JIm[k]}, {k, 1, NH}], 
			Table[Phi[i], {i, 1, NP}] 
		}
	];
	
	functions /. 
	First[NDSolve[Join[equations, conditions], functions, {x, l0, l}]]
]; 

TWTSolverMultiSectionMultiplier[
	b_List, d_List, dl_List, q_List, hList_List, 
	F0_List, CList: {__Real}, 
	{l0_Real, l1_Real, l2_Real, l3_Real}, 
	NF_Integer, NH_Integer, NP_Integer
] := 
Module[
	{solIn, solDrift, solOut, 
	J0In, Phi0In, dPhi0In, CIn, F0In, 
	F0Out, J0Out, Phi0Out, dPhi0Out, COut, 
	J0Drift, Phi0Drift, dPhi0Drift, F, J, Phi}, 
	
	Off[NDSolve::ivres];
	
	J0In = ConstantArray[0.0 I, NH]; 
	Phi0In = Table[j * 2Pi / NP, {j, 0, NP - 1}]; 
	dPhi0In = ConstantArray[0.0, NP]; 
	CIn = CList[[1]]; 
	F0In = F0; 
	
	solIn = 
	TWTSolverSection[b, d, dl, hList[[1]](*{1, 0}*), q, F0In, J0In, Phi0In, dPhi0In, CIn, {l0, l1}, NF, NH, NP]; 
	
	J0Drift = Through[solIn[[2NF + 1 ;; 2NF + 2NH - 1 ;; 2]][l1]] + I Through[solIn[[2NF + 2 ;; 2NF + 2NH ;; 2]][l1]]; 
	Phi0Drift = Through[solIn[[2NF + 2NH + 1 ;; -1]][l1]]; 
	dPhi0Drift = D[Through[solIn[[2NF + 2NH + 1 ;; -1]][x]], x] /. x -> l1; 
	
	solDrift = TWTSolverDrift[q, J0Drift, Phi0Drift, dPhi0Drift, CList[[1]], {l1, l2}, NH, NP]; 
		
	J0Out = Through[solDrift[[1 ;; 2NH - 1 ;; 2]][l2]] + I Through[solDrift[[2 ;; 2NH ;; 2]][l2]]; 
	Phi0Out = Through[solDrift[[2NH + 1 ;; -1]][l2]];
	dPhi0Out = D[Through[solDrift[[2NH + 1 ;; -1]][x]], x] /. x -> l2; 
	COut = CList[[2]]; 
	F0Out = ConstantArray[0.0 I, NF]; 
	
	solOut = 
	TWTSolverSection[b, d, dl, hList[[2]](*{0, 1}*), q, F0Out, J0Out, Phi0Out, dPhi0Out, COut, {l2, l3}, NF, NH, NP]; 

	On[NDSolve::ivres]; 
	
	F = Table[
		Interpolation[
			Table[
				{l, solIn[[i]][l] + I * solIn[[i + 1]][l]},
				{l, l0, l1 - (l3 - l0) / 500.0, (l3 - l0) / 500.0}
			] ~ Join ~ 
			Table[
				{l, 0.0 + 0.0 * I}, 
				{l, l1, l2 - (l3 - l0) / 500.0, (l3 - l0) / 500.0}
			] ~ Join ~
			Table[
				{l, solOut[[i]][l] + I * solOut[[i + 1]][l]},
				{l, l2, l3, (l3 - l0) / 500.0}
			], InterpolationOrder -> 1
		], 
		{i, 1, 2 * NF - 1, 2}
	];
	
	J = Table[
		Interpolation[
			Table[
				{l, solIn[[i]][l] + I * solIn[[i + 1]][l]},
				{l, l0, l1 - (l3 - l0) / 500.0, (l3 - l0) / 500.0}
			] ~ Join ~ 
			Table[
				{l, solDrift[[i - 2 * NF]][l] + I * solDrift[[i - 2 * NF + 1]][l]}, 
				{l, l1, l2 - (l3 - l0) / 500.0, (l3 - l0) / 500.0}
			] ~ Join ~
			Table[
				{l, solOut[[i]][l] + I * solOut[[i + 1]][l]},
				{l, l2, l3, (l3 - l0) / 500.0}
			], 
			InterpolationOrder -> 1
		], 
		{i, 2 * NF + 1, 2 * NF + 2 * NH - 1, 2}
	];
	
	{"F" -> F, "J" -> J}
]; 

Options[TWTParams] := {
	"VBeam" -> 20000.0, 
	"IBeam" -> 0.1, 
	"HBeam" -> 100.0, 
	"WBeam" -> 750.0, 
	"ABeam" -> 100.0, 
	"Nph" -> 3.69, 
	"Freq" -> 100.0, 
	"PowerList" -> {1.0, 0.0}, 
	"CoordList" -> {0.0, 0.02, 0.021, 0.04}, 
	"NH" -> 6, 
	"NP" -> 90, 
	"dataIn" -> Null, 
	"dataOut" -> Null
};

TWTParams[OptionsPattern[]] := {
	"VBeam" -> OptionValue["VBeam"], 
	"IBeam" -> OptionValue["IBeam"], 
	"SBeam" -> OptionValue["HBeam"] * OptionValue["WBeam"] * 10^-12, 
	
	"FreqList" -> OptionValue["Freq"] * {1.0, 2.0} * 10^9, 
	"PowerList" -> OptionValue["PowerList"], 
	"PhaseList" -> {0.0, 0.0}, 
	
	"CoordList" -> OptionValue["CoordList"], 
	
	(* здесь все данные, которые берутся из характеристик записанных в  файлы для 100 и 200 ГГц *)
	"DispersionList" -> {Dispersion[OptionValue["dataIn"]][OptionValue["Freq"] * 10^9], Dispersion[OptionValue["dataOut"]][OptionValue["Freq"] 2.0 * 10^9]}, 
	"RPierceList" -> {RPierce[OptionValue["dataIn"]][OptionValue["Freq"] * 10^9], RPierce[OptionValue["dataOut"]][OptionValue["Freq"] * 2.0 * 10^9]}, 
	"DissipationList" -> {Dissipation[OptionValue["dataIn"]][OptionValue["Freq"] * 10^9], Dissipation[OptionValue["dataOut"]][OptionValue["Freq"] * 2.0 * 10^9]}, 
	
	(* Депрессия для 6 гармоник тока. DeprNum[a, h, V0][k f0] *)
	"DepressionList" -> Map[Depression[OptionValue["HBeam"] * 10^-6, OptionValue["ABeam"] * 10^-6, 
		If[# == OptionValue["Freq"] * 10^9 * 1.0, Dispersion[OptionValue["dataIn"]][OptionValue["Freq"] * 10^9], 
			If[# == OptionValue["Freq"] * 10^9 * 2.0, Dispersion[OptionValue["dataOut"]][OptionValue["Freq"] * 10^9 * 2.0], OptionValue["Nph"]]
		]
		][#]&,  OptionValue["Freq"] * {1.0, 2.0, 3.0, 4.0, 5.0, 6.0} * 10^9], 
	
	"NF" -> 2, 
	"NH" -> OptionValue["NH"], 
	"NP" -> OptionValue["NP"]
}; 

TWTSolverMultiSectionMultiplier[params: {__Rule}] := 
Module[{dimensionlessParams, l0, l1, l2, l3, NF, NH, PNormList, L0, L1, L2, L3, F, J}, 
	dimensionlessParams = TWTDimensionlessParams[params]; 

	L0 = ("CoordList" /. params)[[1]]; 
	L1 = ("CoordList" /. params)[[2]]; 
	L2 = ("CoordList" /. params)[[3]]; 
	L3 = ("CoordList" /. params)[[4]]; 

	l0 = ("lList" /. dimensionlessParams)[[1]]; 
	l1 = ("lList" /. dimensionlessParams)[[2]]; 
	l2 = ("lList" /. dimensionlessParams)[[3]]; 
	l3 = ("lList" /. dimensionlessParams)[[4]]; 
	
	NF = "NF" /. dimensionlessParams; 
	NH = "NH" /. dimensionlessParams; 

	PNormList = "PNormList" /. dimensionlessParams; 

	sol = TWTSolverMultiSectionMultiplier["bList", "dList", "deltaList", "qList", {{1, 0}, {0, 1}}, "F0List", "CList", "lList", "NF", "NH", "NP"] /. 
	dimensionlessParams; 
	F = "F" /. sol; 
	Do[
		F[[i]][[1, 1]] = F[[i]][[1, 1]] * (L3 - l0) / (l3 - l0); 
		F[[i]][[3, 1]] = F[[i]][[3, 1]] * (L3 - l0) / (l3 - l0); 
		F[[i]][[4, 3]] = F[[i]][[4, 3]] * PNormList[[i]];, 
		{i, 1, NF}
	];
	J = "J" /. sol;
	Do[
		J[[i]][[1, 1]] = J[[i]][[1, 1]] * (L3 - l0) / (l3 - l0); 
		J[[i]][[3, 1]] = J[[i]][[3, 1]] * (L3 - l0) / (l3 - l0);,
		{i, 1, NH}
	];
	{"F" -> F, "J" -> J}
]

End[]

SetAttributes[Evaluate[Names["`*"]], ReadProtected]
Protect["`*"]

EndPackage[] (*SectionedTWT`*)
